package com.stcor.ScraperBot.repository;

import com.stcor.ScraperBot.Bots.model.trekkinn.Trekkinn_AvisoHistorico;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 *
 * @author Lucas
 */
@Repository
public interface Trekkinn_AvisoHistoricoRepository extends JpaRepository<Trekkinn_AvisoHistorico,String>{
    Trekkinn_AvisoHistorico findByLink(String link);
}
