package com.stcor.ScraperBot.repository;

import com.stcor.ScraperBot.Bots.model.trekkinn.Trekkinn_Aviso;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 *
 * @author Lucas
 */
@Repository
public interface Trekkinn_AvisoRepository extends JpaRepository<Trekkinn_Aviso,String>{
    Trekkinn_Aviso findByLink(String link);
}
