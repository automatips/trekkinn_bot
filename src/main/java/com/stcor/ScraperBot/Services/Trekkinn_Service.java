package com.stcor.ScraperBot.Services;


import com.stcor.ScraperBot.Bots.model.BasicBot;
import com.stcor.ScraperBot.Bots.model.trekkinn.Trekkinn_Aviso;
import com.stcor.ScraperBot.Bots.model.trekkinn.Trekkinn_AvisoHistorico;
import java.util.Date;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import com.stcor.ScraperBot.repository.Trekkinn_AvisoRepository;
import com.stcor.ScraperBot.repository.Trekkinn_AvisoHistoricoRepository;
import java.util.ArrayList;
import java.util.List;

public class Trekkinn_Service implements BotService {
    
    @Autowired
    private Trekkinn_AvisoRepository avisoRepository;
    
    @Autowired
    public void setTrekkinn_AvisoRepository(Trekkinn_AvisoRepository avisoRepository) {
        
        this.avisoRepository = avisoRepository;
    }
    
    @Autowired
    private Trekkinn_AvisoHistoricoRepository avisoHistoricoRepository;
    
    @Autowired
    public void setTrekkinn_AvisoHistoricoRepository(Trekkinn_AvisoHistoricoRepository avisoHistoricoRepository) {
        this.avisoHistoricoRepository = avisoHistoricoRepository;
    }
    
    @Override
    public void start(){            
        BasicBot bot = new BasicBot();
        
        bot.InicializarWebdriver();
        
        goToCategory(bot);
        
        List<Trekkinn_Aviso> lista = getAvisos(bot);
        
        getAvisosPrecioEnvio(bot, lista);
    }
    
    protected List<Trekkinn_Aviso> getAvisos(BasicBot bot){
        List<Trekkinn_Aviso> listaAvisos = new ArrayList<Trekkinn_Aviso>();
        scroll(bot);
        boolean obj_correcto = true;
        System.out.println(new Date()+": Inicializando lectura");
        
        int indexAviso = 1;
            while (obj_correcto){
                Trekkinn_Aviso aviso = new Trekkinn_Aviso();
                aviso = getAviso(bot, indexAviso);
                if (aviso.getLink() != null){                    
                    listaAvisos.add(aviso);
                    indexAviso++;
                }else{
                    obj_correcto = false;
                }        
        }
            
        return listaAvisos;
    }
    
    public Trekkinn_Aviso getAviso(BasicBot bot, int indexAviso){
        Trekkinn_Aviso aviso = new Trekkinn_Aviso();
        try{
            WebElement weAviso =

            //Name text href
            weAviso = bot.Xpath("/html/body/div[7]/div/div[3]/div[6]/ul/li["+indexAviso+"]/div/div[2]/p[1]/a");
            try{
                System.out.println(new Date()+": Obteniendo nombre ID "+indexAviso);
                aviso.setNombre(weAviso.getText());
                System.out.println(new Date()+": Nombre >>> "+aviso.getNombre());
                aviso.setLink(weAviso.getAttribute("href"));
                System.out.println(new Date()+": Link >>> "+aviso.getLink());

            }catch(Exception ex){
                System.out.println(new Date()+": ERROR >>> " +ex.getMessage());
            }

            //Image src
            weAviso = bot.Xpath("/html/body/div[7]/div/div[3]/div[6]/ul/li["+indexAviso+"]/div/div[1]/a/img");
            try{
                System.out.println(new Date()+": Obteniendo imagen ID "+indexAviso);
                aviso.setFoto(weAviso.getAttribute("src"));
                System.out.println(new Date()+": Foto >>> "+aviso.getFoto());
            }catch(Exception ex){
                System.out.println(new Date()+": ERROR >>> " +ex.getMessage());
            }

            //Precio tachado
            weAviso = bot.Xpath("/html/body/div[7]/div/div[3]/div[6]/ul/li["+indexAviso+"]/div/div[2]/p[2]/span");
            try{
                System.out.println(new Date()+": Obteniendo precio tachado ID "+indexAviso);
                aviso.setPrecioTach(weAviso.getText());
                System.out.println(new Date()+": PrecioTach >>> "+aviso.getPrecioTach());
            }catch(Exception ex){
                System.out.println(new Date()+": ERROR >>> " +ex.getMessage());
            }

            //Precio neto
            weAviso = bot.Xpath("/html/body/div[7]/div/div[3]/div[6]/ul/li["+indexAviso+"]/div/div[2]/p[2]/text()");
            try{
                System.out.println(new Date()+": Obteniendo precio neto ID "+indexAviso);
                aviso.setPrecioNeto(weAviso.getText());
                System.out.println(new Date()+": PrecioNeto >>> "+aviso.getPrecioNeto());

            }catch(Exception ex){
                System.out.println(new Date()+": ERROR >>> " +ex.getMessage());
            }

            //Talla
            weAviso = bot.Xpath("/html/body/div[7]/div/div[3]/div[6]/ul/li["+indexAviso+"]/div/div[2]/div/ul/li[2]/a");
            try{
                System.out.println(new Date()+": Obteniendo talla ID "+indexAviso);
                aviso.setTalla(weAviso.getText());
                System.out.println(new Date()+": Talla >>> "+aviso.getTalla());
            }catch(Exception ex){
                System.out.println(new Date()+": ERROR >>> " +ex.getMessage());
            }

            return aviso;
        }
        catch(Exception ex){
            return aviso;
        }
    }
    
    public void getAvisosPrecioEnvio(BasicBot bot, List<Trekkinn_Aviso> listaAvisos){
        for (Trekkinn_Aviso aviso: listaAvisos) {
            
            Trekkinn_Aviso avisoListo = getAvisoPrecioEnvio(bot,aviso);
            
            Trekkinn_Aviso aviso_consultar = avisoRepository.findByLink(avisoListo.getLink());
            System.out.println(new Date()+": Aviso con link: "+avisoListo.getLink());               

            Trekkinn_Aviso avisoPersistir = new Trekkinn_Aviso();

            if (aviso_consultar == null){
                avisoListo.setAlta(new Date());
                avisoListo.setUltimaActualizacion(new Date());
                avisoPersistir = avisoListo;
            }
            else{
                avisoPersistir = aviso_consultar;

                avisoPersistir.setPrecioNeto(avisoListo.getPrecioNeto());
                avisoPersistir.setPrecioTach(avisoListo.getPrecioTach());
                avisoPersistir.setPrecioEnvio(avisoListo.getPrecioEnvio());
                avisoPersistir.setUltimaActualizacion(new Date());

                Trekkinn_AvisoHistorico ah = new Trekkinn_AvisoHistorico();
                ah.setAlta(aviso_consultar.getAlta());
                ah.setNombre(aviso_consultar.getNombre());
                ah.setLink(aviso_consultar.getLink());
                ah.setFoto(aviso_consultar.getFoto());
                ah.setPrecioTach(aviso_consultar.getPrecioTach());
                ah.setPrecioNeto(aviso_consultar.getPrecioNeto());
                ah.setPrecioEnvio(aviso_consultar.getPrecioEnvio());
                ah.setUltimaActualizacion(aviso_consultar.getUltimaActualizacion());

                //Almacenamos un registro para mantener el seguimiento de los cambios
                avisoHistoricoRepository.save(ah);
            }
            avisoRepository.save(avisoPersistir);
        }
    }
    
    public Trekkinn_Aviso getAvisoPrecioEnvio(BasicBot bot, Trekkinn_Aviso aviso){
        try{
            bot.IrA(aviso.getLink());
            Thread.sleep(5000);
            bot.ScrollDown();
            
            WebElement weAviso = null;                    
            
            try{
                System.out.println(new Date()+": Añadiendo al carrito "+aviso.getNombre());
                
                //Añadir a la cesta
                weAviso = bot.Xpath("/html/body/div[8]/div/div[5]/div[1]/div[2]/div[3]/form/div[5]/input");
                weAviso.click();
                
                Thread.sleep(5000);
                
                //Ver cesta
                weAviso = bot.Xpath("/html/body/div[1]/div[1]/div[3]/div[18]/div[1]/div/p/a");
                weAviso.click();
                
                Thread.sleep(3000);
                
                //obtener precio de envio
                weAviso = bot.Xpath("/html/body/div[7]/div/div[2]/div[3]/form/div[1]/div[2]/p");
                aviso.setPrecioEnvio(weAviso.getText());
                
                //limpiar cesta
                weAviso = bot.Xpath("/html/body/div[7]/div/div[2]/div[3]/div[1]/div[2]/div[2]/a[3]");
                weAviso.click();

            }catch(Exception ex){
                System.out.println(new Date()+": ERROR >>> " +ex.getMessage());
            }
            
            return aviso;
        }
        catch(Exception ex){
            return aviso;
        }        
    }
    
    protected void goToCategory(BasicBot bot){
        try{
            bot.IrA("https://www.trekkinn.com/montana/calzado-hombre-accesorios/3034/s#fq=id_familia%3A3012&fq={!tag=ds}id_subfamilia%3A3034&fq=id_tienda%3A3&sort=v30+desc,product(tm3,%20dispo)+asc&start=48");
            Thread.sleep(10000);
        }
        catch(Exception ex){
            
        }
    }    
    
    public void scroll(BasicBot bot){
        try{
            bot.ScrollDown();
            Thread.sleep(1000);
            
            bot.ScrollDown();
            Thread.sleep(1000);
            
            bot.ScrollDown();
            Thread.sleep(1000);
            
            bot.ScrollDown();
            Thread.sleep(1000);
            
            bot.ScrollDown();
            Thread.sleep(1000);
            
            bot.ScrollDown();
            Thread.sleep(1000);
            
            bot.ScrollDown();
            Thread.sleep(1000);
            
            bot.ScrollDown();
            Thread.sleep(1000);
            
            bot.ScrollDown();
            Thread.sleep(1000);
            
            bot.ScrollDown();
            Thread.sleep(1000);
        }
        catch(Exception ex){
            
        }
    }
}
