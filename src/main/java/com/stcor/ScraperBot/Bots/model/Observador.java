package com.stcor.ScraperBot.Bots.model;

import org.openqa.selenium.WebElement;

import java.util.*;

public class Observador {
    public String xpath;
    public boolean clickear;
    public boolean jsClickear;
    public int timeout;
    public int reintentarCada;
    public int reintentos;
    public int scrollearVeces;
    public boolean enfocar;
    public String altXpath;
    public LinkedList<String[]> jsExecutorScripts ;

    public Observador(String xpath){
        this.xpath = xpath;
        this.clickear = BasicBot.DEFAULT_CLICKEAR;
        this.jsClickear = BasicBot.DEFAULT_JSCLICKEAR;
        this.timeout = BasicBot.DEFAULT_TIMEOUT;
        this.reintentarCada = BasicBot.DEFAULT_REINTENTAR_CADA;
        this.reintentos = BasicBot.DEFAULT_REINTENTOS;
        this.scrollearVeces = BasicBot.DEFAULT_SCROLLEAR_VECES;
        this.enfocar = BasicBot.DEFAULT_ENFOCAR;
        this.altXpath = BasicBot.DEFAULT_ALTXPATH;
        this.jsExecutorScripts = new LinkedList<>();


    }

    public void addJsExecutor(String s, String xpath) {
        jsExecutorScripts.add(new String[]{s, xpath});
    }
    public void addJsExecutor(String s) {
        jsExecutorScripts.add(new String[]{s, null});
    }
}
