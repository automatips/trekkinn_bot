package com.stcor.ScraperBot.Bots.model;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.ProfilesIni;
import org.openqa.selenium.interactions.Actions;

import java.io.Serializable;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;


//@Entity
public class BasicBot implements Serializable{

    public static final int DEFAULT_TIMEOUT = 0 ;
    public static final int DEFAULT_SCROLLEAR_VECES = 0 ;
    public static final int DEFAULT_REINTENTOS = 10 ;
    public static final int DEFAULT_REINTENTAR_CADA = 10 ;
    public static final boolean DEFAULT_ENFOCAR = false;
    public static final boolean DEFAULT_CLICKEAR = false;
    public static final boolean DEFAULT_JSCLICKEAR = false;
    public static final String DEFAULT_ALTXPATH = "";
    //  @Id
  //  @GeneratedValue(strategy = GenerationType.AUTO, generator = "system-uuid")
  //  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;
    private Date alta;

  //  @Transient
    private WebDriver driver;
    private String ip;

    private String urlActual;
    private boolean headless = false;
    private LinkedList<Observador> Observadores;
    private String IdMainTab;


    public void GetIp(){
        try(final DatagramSocket socket = new DatagramSocket()){
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            this.ip = socket.getLocalAddress().getHostAddress();
        }catch (Exception e){

        }
    }


    public BasicBot(){
        this.Observadores = new LinkedList<>();
        WebDriverManager.firefoxdriver().setup();
        GetIp();

    }
    public void InicializarWebdriver(){
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        ProfilesIni profileIni = new ProfilesIni();
        FirefoxProfile profile = profileIni.getProfile("default-release");
        profile.setPreference("dom.webnotifications.enabled", false);
        profile.setPreference("dom.push.enabled", false);
        profile.setAcceptUntrustedCertificates(true);

        profile.setPreference("capability.policy.default.HTMLDocument.readyState", "allAccess");
        profile.setPreference("capability.policy.default.HTMLDocument.compatMode", "allAccess");
        profile.setPreference("capability.policy.default.Document.compatMode", "allAccess");
        profile.setPreference("capability.policy.default.Location.href", "allAccess");
        profile.setPreference("capability.policy.default.Window.pageXOffset", "allAccess");
        profile.setPreference("capability.policy.default.Window.pageYOffset", "allAccess");
        profile.setPreference("capability.policy.default.Window.frameElement", "allAccess");
        profile.setPreference("capability.policy.default.Window.frameElement.get", "allAccess");
        profile.setPreference("capability.policy.default.Window.QueryInterface", "allAccess");
        profile.setPreference("capability.policy.default.Window.mozInnerScreenY", "allAccess");
        profile.setPreference("capability.policy.default.Window.mozInnerScreenX", "allAccess");



        firefoxOptions.setProfile(profile);
        firefoxOptions.setCapability("locationContextEnabled", false);

        firefoxOptions.addPreference("permissions.default.geo",2);
        firefoxOptions.addPreference("permissions.default.desktop-notificatio",2);

        if(this.headless){
            firefoxOptions.addArguments("--headless");
        }
        this.driver = new FirefoxDriver(firefoxOptions);
        driver.get("http://www.google.com");
        IdMainTab = driver.getWindowHandle();
    }
    public void IrA(String url)
    {
        try
        {
            if(url != null){
                driver.get(url);
            }else{
                driver.get("http://www.google.com/search?q="+url.replace(" ","+"));
            }
        }
        catch (Exception e)
        {
            System.out.println("ERROR: "+e.toString());
        }
    }

    //(string xpath, int INTERVALO = 2, int TIMEOUT = -1, int REINTENTOS = 10, bool CLICKEAR = false, int SCROLLDOWN= 0, bool MOVETO = false, string ALTERNATIVEXPATH ="", bool JSCLICK = false)

    public boolean EsperarPorEnfocarReintentar(String xpath, boolean enfocar, int reintentos, int reintentar_cada) {
        int TIMEOUT = DEFAULT_TIMEOUT;
        int REINTENTAR_CADA = reintentar_cada;
        int REINTENTOS = reintentos;
        int SCROLLEAR_VECES = DEFAULT_SCROLLEAR_VECES;
        boolean ENFOCAR = enfocar;
        boolean CLICKEAR = DEFAULT_CLICKEAR;
        boolean JSCLICKEAR = DEFAULT_JSCLICKEAR;
        String ALTXPATH = DEFAULT_ALTXPATH;
        return EsperarPor(xpath,TIMEOUT,REINTENTAR_CADA,REINTENTOS,SCROLLEAR_VECES,ENFOCAR,CLICKEAR,JSCLICKEAR,ALTXPATH);

    }

    public boolean EsperarPor(String xpath){
        int TIMEOUT = DEFAULT_TIMEOUT;
        int REINTENTAR_CADA = DEFAULT_REINTENTAR_CADA;
        int REINTENTOS = DEFAULT_REINTENTOS;
        int SCROLLEAR_VECES = DEFAULT_SCROLLEAR_VECES;
        boolean ENFOCAR = DEFAULT_ENFOCAR;
        boolean CLICKEAR = DEFAULT_CLICKEAR;
        boolean JSCLICKEAR = DEFAULT_JSCLICKEAR;
        String ALTXPATH = DEFAULT_ALTXPATH;
        return EsperarPor(xpath,TIMEOUT,REINTENTAR_CADA,REINTENTOS,SCROLLEAR_VECES,ENFOCAR,CLICKEAR,JSCLICKEAR,ALTXPATH);

    }
    public boolean EsperarPorEscrollearClickear(String xpath,int SCROLLEAR_VECES, boolean CLICKEAR) {
        int TIMEOUT = DEFAULT_TIMEOUT;
        int REINTENTAR_CADA = DEFAULT_REINTENTAR_CADA;
        int REINTENTOS = DEFAULT_REINTENTOS;
        boolean ENFOCAR = DEFAULT_ENFOCAR;
        boolean JSCLICKEAR = DEFAULT_JSCLICKEAR;
        String ALTXPATH = DEFAULT_ALTXPATH;
        return EsperarPor(xpath,TIMEOUT,REINTENTAR_CADA,REINTENTOS,SCROLLEAR_VECES,ENFOCAR,CLICKEAR,JSCLICKEAR,ALTXPATH);
    }

    public boolean EsperarPorEscrollearJSClickear(String xpath,int SCROLLEAR_VECES, boolean JSCLICKEAR) {
        int TIMEOUT = DEFAULT_TIMEOUT;
        int REINTENTAR_CADA = DEFAULT_REINTENTAR_CADA;
        int REINTENTOS = DEFAULT_REINTENTOS;
        boolean ENFOCAR = DEFAULT_ENFOCAR;
        boolean CLICKEAR = DEFAULT_CLICKEAR;
        String ALTXPATH = DEFAULT_ALTXPATH;
        return EsperarPor(xpath,TIMEOUT,REINTENTAR_CADA,REINTENTOS,SCROLLEAR_VECES,ENFOCAR,CLICKEAR,JSCLICKEAR,ALTXPATH);

    }

        public boolean EsperarPorReintentarClickear(String xpath,int REINTENTOS,int REINTENTAR_CADA, boolean CLICKEAR, boolean JSCLICKEAR){
        int TIMEOUT = DEFAULT_TIMEOUT;
        int SCROLLEAR_VECES = DEFAULT_SCROLLEAR_VECES;
        boolean ENFOCAR = DEFAULT_ENFOCAR;
        String ALTXPATH = DEFAULT_ALTXPATH;
        return EsperarPor(xpath,TIMEOUT,REINTENTAR_CADA,REINTENTOS,SCROLLEAR_VECES,ENFOCAR,CLICKEAR,JSCLICKEAR,ALTXPATH);

    }
    public boolean EsperarPorClickear(String xpath,String ALTXPATH,boolean CLICKEAR,boolean JSCLICKEAR ){
        int TIMEOUT = DEFAULT_TIMEOUT;
        int REINTENTAR_CADA = DEFAULT_REINTENTAR_CADA;
        int REINTENTOS = DEFAULT_REINTENTOS;
        int SCROLLEAR_VECES = DEFAULT_SCROLLEAR_VECES;
        boolean ENFOCAR = DEFAULT_ENFOCAR;
        return EsperarPor(xpath,TIMEOUT,REINTENTAR_CADA,REINTENTOS,SCROLLEAR_VECES,ENFOCAR,CLICKEAR,JSCLICKEAR,ALTXPATH);

    }

    public boolean EsperarPorTimeoutClickear(String xpath, int TIMEOUT, int REINTENTAR_CADA, int REINTENTOS,boolean CLICKEAR, boolean JSCLICKEAR){
        int SCROLLEAR_VECES = DEFAULT_SCROLLEAR_VECES;
        boolean ENFOCAR = DEFAULT_ENFOCAR;
        String ALTXPATH = DEFAULT_ALTXPATH;

        return EsperarPor(xpath,TIMEOUT,REINTENTAR_CADA,REINTENTOS,SCROLLEAR_VECES,ENFOCAR,CLICKEAR,JSCLICKEAR,ALTXPATH);

    }
    public boolean EjecutarObjservador(Observador observador){

        String xpath = observador.xpath;
        boolean clickear = observador.clickear;
        boolean jsClickear = observador.jsClickear;
        int timeout = observador.timeout;
        int reintentarCada = observador.reintentarCada;
        int reintentos = observador.reintentos;
        int scrollearVeces = observador.scrollearVeces;
        boolean enfocar = observador.enfocar;
        String altXpath = observador.altXpath;

        boolean result = EsperarPor(xpath,timeout,reintentarCada,reintentos,scrollearVeces,enfocar,clickear, jsClickear,altXpath,true);
        for (String[] scriptArray : observador.jsExecutorScripts) {
            String script = scriptArray[0].toString();
            String args = null;
            try{
                args = scriptArray[1];

            }catch (Exception ex){
                System.out.println(ex);
            }
            if(args!=null){
                try{
                    this.EjecutarScript(script,args);

                }catch (Exception ex){
                    System.out.println(ex.getStackTrace());
                }
            }else{
                try{
                    this.EjecutarScript(script);

                }catch (Exception ex){
                    System.out.println(ex);
                }

            }
        }
        return result;
    }

    private boolean EsperarPor(String xpath, int timeout, int reintentarCada, int reintentos, int scrollearVeces, boolean enfocar, boolean clickear, boolean jsClickear, String altXpath) {
        return EsperarPor(xpath,timeout,reintentarCada,reintentos,scrollearVeces,enfocar,clickear, jsClickear,altXpath,false);

    }

    public boolean EsperarPor(
            String xpath,
            int TIMEOUT,
            int REINTENTAR_CADA,
            int REINTENTOS,
            int SCROLLEAR_VECES,
            boolean ENFOCAR,
            boolean CLICKEAR,
            boolean JSCLICKEAR,
            String ALTXPATH,
            boolean esObservador
    ){

        boolean PRIMER_INTENTO = true; //necesario para q funcione el timeout en -1
        int SCROLL_SIZE = 0;
        boolean result = false;
        LocalDateTime startTime = LocalDateTime.now();

        while( (REINTENTOS > 0 && Duration.between(startTime, LocalDateTime.now()).getSeconds() > TIMEOUT ) || PRIMER_INTENTO)
        {
            PRIMER_INTENTO = false;
            try{

                while (SCROLLEAR_VECES > 0)
                {
                    SCROLL_SIZE = SCROLL_SIZE + 350;
                    try
                    {
                        JavascriptExecutor js = (JavascriptExecutor) driver;
                        js.executeScript("window.scrollBy(0," + SCROLL_SIZE + ")");
                        this.driver.findElements(By.xpath(xpath));
                        break;
                    }
                    catch(Exception ex)
                    {

                    }
                    SCROLLEAR_VECES = SCROLLEAR_VECES - 1;
                }

                if(ENFOCAR){
                    Enfocar(xpath);
                }

                if (JSCLICKEAR)
                {
                    JavascriptExecutor js = (JavascriptExecutor)this.driver;
                    String script1 = "var a = document.evaluate(arguments[0], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;a.click();";
                    Object a = js.executeScript(script1, xpath);
                    if (a != null)
                    {
                        result = true;
                        break;
                    }

                }
                else
                {
                    if (CLICKEAR){
                        this.driver.findElement(By.xpath(xpath)).click();
                    }
                }

                if(esObservador){
                    this.driver.findElement(By.xpath(xpath));

                }else {
                    this.Xpath(xpath);
                    result = true;
                }
                break;



            }catch (Exception ex){

                if (ALTXPATH != "")
                {
                    try
                    {
                        this.Xpath(ALTXPATH);
                        result = false; //todo ok, pero se encontró el xpath alternativo
                        break;
                    }
                    catch(Exception exx) { }
                }

            }

            try { Thread.sleep(REINTENTAR_CADA * 1000); } catch (InterruptedException e) { e.printStackTrace(); }
            REINTENTOS = REINTENTOS - 1;
        }


        return result;
    }

    public void Enfocar(WebElement we) {
        try
        {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].scrollIntoView(true);",we);
            try { Thread.sleep(500); } catch (InterruptedException e) { e.printStackTrace(); }

        }
        catch(Exception ex)
        {

        }
    }

    public void Enfocar(String xpath) {
        try
        {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].scrollIntoView(true);",this.driver.findElement(By.xpath(xpath)));
            try { Thread.sleep(500); } catch (InterruptedException e) { e.printStackTrace(); }

        }
        catch(Exception ex)
        {

        }
    }


    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the alta
     */
    public Date getAlta() {
        return alta;
    }

    /**
     * @param alta the alta to set
     */
    public void setAlta(Date alta) {
        this.alta = alta;
    }


    public void EjecutarScript(String script) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(script);
    }

    public void EjecutarScript(String script, String argument) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement e = this.driver.findElement(By.xpath(argument));
        js.executeScript(script,e);
    }

    public WebElement Xpath(WebElement element, String xpath) {
        for (Observador observador: Observadores) {
            EjecutarObjservador(observador);
        }
        try{ return element.findElement(By.xpath(xpath)); } catch (NotFoundException nf){
            return null;
        }

    }
    public WebElement Xpath(String path) {
        for (Observador observador: Observadores) {
            EjecutarObjservador(observador);
        }
        try{ return this.driver.findElement(By.xpath(path)); } catch (NotFoundException nf){
            return null;
        }

    }
    public WebElement[] Xpaths(WebElement element, String xpath) {
        for (Observador observador: Observadores) {
            EjecutarObjservador(observador);
        }
        try{ return element.findElements(By.xpath(xpath)).toArray(new WebElement[0]); } catch (NotFoundException nf){
            return new WebElement[0];
        }
    }
    public List<WebElement> Xpaths(String path) {
        for (Observador observador: Observadores) {
            EjecutarObjservador(observador);
        }
        return this.driver.findElements(By.xpath(path));
    }

    public void AgregarObservador(Observador observador) {
        this.Observadores.add(observador);
    }




    public void ControlClick(WebElement element) {

        Actions actions = new Actions(driver);
        actions.keyDown(Keys.CONTROL)
                .click(element)
                .keyUp(Keys.CONTROL)
                .build()
                .perform();


    }

    public void SiguienteTab() {

        ArrayList tabList = GetTabList();
        int posicionActual = tabList.indexOf(driver.getWindowHandle());
        if(tabList.size() > posicionActual + 1){
            driver.switchTo().window((String)tabList.get(posicionActual + 1));
        }else{
            driver.switchTo().window(IdMainTab);
        }

    }

    public ArrayList GetTabList(){
        return new ArrayList(driver.getWindowHandles());
    }

    public void TabPrincipal(){
        driver.switchTo().window(this.IdMainTab);
        driver.switchTo().window((String)GetTabList().get(0));
    }

    public void NewTab(String url) {
        EjecutarScript("window.open('"+url+"', '_blank')");
    }
    /**
     * Cierra el webdriver
     */
    public void Quit() {
        driver.quit();
    }

    /**
     * Cierra la ventana/tab en foco
     */
    public void Close() {
        driver.close();
    }
    
    public void ScrollDown(){
        EjecutarScript("window.scrollBy(0,900)");
    }    
}
